import org.junit.jupiter.api.Test;

import static java.lang.Math.sqrt;
import static org.junit.jupiter.api.Assertions.*;

class Vector3DTest {

    @Test
    void testGetLen() {
        Vector3D vector = new Vector3D(3, 4, 5);
        assertEquals(sqrt(50), vector.getLen());
    }

    @Test
    void testEqualsWithSameVectors() {
        Vector3D a = new Vector3D(3, 4, 5);
        Vector3D b = new Vector3D(3, 4, 5);
        assertTrue(Vector3D.equals(a, b));

    }

    @Test
    void testEqualsWithDifferentVectors() {
        Vector3D a = new Vector3D(0, 0, 0);
        Vector3D b = new Vector3D(3, 4, 5);
        assertFalse(Vector3D.equals(a, b));
    }

}