class Point3DTest {
    public static void main(String[] args) {
        Point3D point1 = new Point3D(1, 2, 3);
        Point3D point2 = new Point3D(1, 2, 3);
        Point3D point3 = new Point3D(3, 6, -7);
        Point3D point4 = new Point3D();
        Point3D point5 = new Point3D();

        if (point1 == point2) {
            System.out.println("Точки равны");
        }
        else{
            System.out.println("Точки не равны");
        }

        if (point4 == point5) {
            System.out.println("Точки равны");
        } else {
            System.out.println("Точки не равны");
        }

        if (point3 == point3) {
            System.out.println("Точки равны");
        } else {
            System.out.println("Точки не равны");
        }

        point1.printPoint();

    }
}