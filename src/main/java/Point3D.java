/**
 * Класс Point3D (точка в трехмерном пространстве).Вектор хранится
 * в виде набора своих координат
 */
public class Point3D {
    /**
     * Поле-координата x
     */
    private double x;
    /**
     * Поле-координата y
     */
    private double y;
    /**
     * Поле-координата z
     */
    private double z;

    /**
     * Конструктор - создание новой точки
     *
     * @param x - координата {@link Point3D#x}
     * @param y - координата {@link Point3D#y}
     * @param z - координата {@link Point3D#z}
     * @see Point3D#Point3D()
     */
    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Конструктор без параметров (создает точку – начало координат)
     */
    public Point3D() {
        this(0, 0, 0);
    }

    /**
     * Процедура изменения координаты {@link Point3D#x}
     *
     * @param x - координата x
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Процедура изменения координаты {@link Point3D#y}
     *
     * @param y - координата y
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Процедура изменения координаты {@link Point3D#z}
     *
     * @param z - координата z
     */
    public void setZ(double z) {
        this.z = z;
    }

    /**
     * Процедура изменения координат {@link Point3D#x},{@link Point3D#y},{@link Point3D#z}
     *
     * @param x - координата x
     * @param y - координата y
     * @param z - координата z
     */
    public void setXYZ(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Функция получения значения поля {@link Point3D#x}
     *
     * @return возвращает координату x
     */
    public double getX() {
        return x;
    }

    /**
     * Функция получения значения поля {@link Point3D#y}
     *
     * @return возвращает координату y
     */
    public double getY() {
        return y;
    }

    /**
     * Функция получения значения поля {@link Point3D#z}
     *
     * @return возвращает координату z
     */
    public double getZ() {
        return z;
    }

    /**
     * Функция выводит координты точки на консоль
     */
    public void printPoint() {
        System.out.printf("x:%f y:%f z:%f\n", x, y, z);
    }

    /**
     * Функция проверяет равенство точки с переданной(или объектом другого типа)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point3D point3D = (Point3D) o;
        return Double.compare(point3D.x, x) == 0 &&
                Double.compare(point3D.y, y) == 0 &&
                Double.compare(point3D.z, z) == 0;
    }


}